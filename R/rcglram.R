#' Robust Constrained GLRAM
#'
#' Fits a robust constrained generalized low rank approximation of matrices
#' Y = PVD'
#' where D is a fixed orthonormal matrix, and the objective is based on the
#' L1 norm of the reconstruction error.
#'
#' @param tnsr A 3-Tensor, where the third mode is the observation mode.
#' @param D An orthonormal matrix. Number of rows must match the second mode of tnsr.
#' @param prank Rank of P
#' @param mu Initial mu
#' @param mubar Maximum mu
#' @param rho Mu step
#' @param epsilon Convergence tolerance
#' @param maxiter Maximum number of iterations

rcglram <- function(tnsr, D, prank, mu = 1e-4, mubar = 1e10, rho = 1.1,
                    epsilon = 1e-5, maxiter = 1000) {

    # Check for proper arguments
    if (tnsr@num_modes != 3) {
        stop("Input must be a 3-tensor")
    }
    if (sum(tnsr@data == 0) == prod(tnsr@modes)) {
        stop("Input must not be the zero tensor")
    }
    if (prank <= 0) {
        stop("prank must be positive")
    }
    if (prank > tnsr@modes[1]) {
        stop("prank must be less than or equal to first mode of tnsr")
    }

    # Check that dimension of D is compatible with tnsr
    if (nrow(D) != tnsr@modes[2]) {
        stop('Rows of D must match second mode of tnsr')
    }

    # Check that D is orthonormal
    if(!all.equal(t(D) %*% D, diag(ncol(D)))) {
        stop('D is not orthonormal')
    }

    # tnsr2list
    Y <- lapply(1:tnsr@modes[3], function(i) tnsr@data[,,i])
    n <- length(Y)
    nr <- nrow(Y[[1]])
    nc <- prank

    # Initialize L
    L <- orthonormalization(matrix(rnorm(nr * nc), nr, nc), basis = F)

    # Initialize others...
    M <- lapply(Y, function(i) t(L) %*% i %*% D)
    E <- lapply(Y, function(i) 0 * i)
    X <- lapply(Y, function(i) 0 * i)

    # Error
    normD <- sqrt(sum(sapply(Y, function(i) norm(i, type = 'F')^2)))

    # Iterate
    iter <- 0
    flag <- T
    errlog <- numeric(0)
    while (iter <= maxiter & flag) {

        # Update L
        L1 <- matrix(0, nr, nc)
        for (i in 1:n) {
            L1 <- L1 + (Y[[i]] - E[[i]] + X[[i]]/mu) %*% D %*% t(M[[i]])
        }
        L <- qr.Q(qr(L1))

        # Update M
        for (i in 1:n) {
            M[[i]] <- t(L) %*% (Y[[i]] - E[[i]] + X[[i]]/mu) %*% D
        }

        # Update E
        for (i in 1:n) {
            EE     <- Y[[i]] + X[[i]] / mu - L %*% M[[i]] %*% t(D)
            E[[i]] <- pmax(EE - 1/mu, 0)
            E[[i]] <- E[[i]] + pmin(EE + 1/mu, 0)
        }

        # Update X
        normErr <- 0
        for (i in 1:n) {
            temp    <- Y[[i]] - L %*% M[[i]] %*% t(D) - E[[i]]
            X[[i]]  <- X[[i]] + mu * temp
            normErr <- normErr + norm(temp, type = 'F')^2
        }
        normErr <- sqrt(normErr)
        errlog <- c(errlog, normErr/normD)

        # Update mu
        mu <- min(rho*mu, mubar)
        if (normErr/normD < epsilon){
            flag <- F
        }
        iter <- iter + 1
    }

    P <- L
    V <- M

    # Explained variance
    exp.var <- function(Y, V, P, D) {

        est  <- sapply(1:n, function(i) P %*% V[[i]] %*% t(D), simplify = 'array')
        tnsr <- sapply(Y, function(i) i, simplify = 'array')

        est <- as.tensor(est)
        tnsr <- as.tensor(tnsr)
        fnorm_resid <- fnorm(est - tnsr)
        return((1 - (fnorm_resid/fnorm(tnsr))) * 100)
    }

    evar <- exp.var(Y, V, P, D)

    ret <- list(P = P, V = V, D = D, E = E, evar = evar, errlog = errlog)
    return(ret)
}
