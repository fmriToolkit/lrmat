# lrmat

Implements several forms of low rank matrix approximation, including an updated and expanded version of the Population Value Decomposition (PVD) implemented by rTensor and proposed by Crainiceanu et al (2013). 
Includes options for robust SVD and estimation of factor ranks by cross validation. The package also implements a modified version of PVD which scales the singular vectors of each observation by their singular values, placing greater weight on those singular vectors which explain a larger proportion of the variance in each observation.
Much of the functionality has been placed on top of existing code in the PVD implementation in rTensor, and is designed to be as consistent with rTensor as possible.
The input/output of pvd2() is essential identical to that of rTensor::pvd(), with the only major exception being that the output of pvd2() is now implemented as an S3 class with it's own predict() method.
In particular, the default options match those of rTensor, so that the following commands should return identical output:
```r
lrmat::pvd2(Y, uranks, wranks, a, b)
rTensor::pvd(Y, uranks, wranks, a, b)
```

The package also implements a constrained version of the generalized low rank approximation of matrices (GLRAM; Ye, J., 2005)
```
Y_i = P V_i D' 
```
in which D is constrained to be a fixed, orthonormal matrix.
Also included is a robust version based on the minimization of the L1 reconstruction error (Shi, et al., 2015)

Note that the package is so far pretty sparsely documented.

<h3>References</h3>
Crainiceanu, C. M., Caffo, B. S., Luo, S., Zipunnikov, V. M., & Punjabi, N. M. (2011). Population value decomposition, a framework for the analysis of image populations. Journal of the American Statistical Association, 106(495), 775-790.

Li J, Bien J, Wells MT (2018). “rTensor: An R Package for Multidimensional Array
(Tensor) Unfolding, Multiplication, and Decomposition.” _Journal of Statistical
Software_, *87*(10), 1-31. doi: 10.18637/jss.v087.i10 (URL:
http://doi.org/10.18637/jss.v087.i10).

Shi, J., Yang, W., & Zheng, X. (2015). Robust generalized low rank approximations of matrices. Plos one, 10(9), e0138028.

Ye, J. (2005). Generalized low rank approximations of matrices. Machine Learning, 61(1-3), 167-191.

<h3>Installation</h3>
The package can be installed directly from the Gitlab repository using the `devtools` package with

```r
devtools::install_git('https://gitlab.com/fmriToolkit/lrmat.git')
```

Alternately, the repository can be cloned to a local machine and installed through the terminal by running

```
R CMD INSTALL lrmat
```
in the directory containing the folder.